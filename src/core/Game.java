package core;

import javafx.application.Application;
import javafx.stage.Stage;
/**
 * 
 * @author loren
 * Main-Thread - GUI 
 */
public class Game extends Application {

    public static void main(final String... args) {
        Application.launch();
    }

    @Override
    public final void start(final Stage primaryStage) throws Exception {
    	new GameLoop().start();
	}


}


