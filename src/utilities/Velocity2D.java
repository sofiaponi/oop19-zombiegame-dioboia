package utilities;

public interface Velocity2D {
	
	/**
	 * 
	 * @return X component
	 */
	double getVelX();
	
	/**
	 * 
	 * @return Y component
	 */
	double getVelY();
	
	/**
	 * 
	 * @return Vector's module
	 */
	double getModule();
	
	/**
	 * Normalizes the vector's instance
	 * @return The normalized vector
	 */
	Velocity2D getNormalized();
	
	/**
	 * Sets velocity to [0,0]
	 */
	void stop();
	
	/**
	 * Updates to a new velocity
	 * @param newVelocity New velocty to be assigned
	 */
	void updateVelocity(Velocity2D newVelocity);
}
