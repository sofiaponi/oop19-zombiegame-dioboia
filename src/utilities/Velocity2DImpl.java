package utilities;

import javafx.geometry.Point2D;

public class Velocity2DImpl implements Velocity2D {
	
	/**
	 * Velocity's representation as a vector from [0,0] to velocity's coordinates
	 */
	private Point2D velocity;

	public Velocity2DImpl(Point2D distance) {
		this.velocity = distance;
	}
	
	public Velocity2DImpl(final double x, final double y) {
		this.Velocity2DImpl(new Point2D(x, y));
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public double getVelX() {
		return velocity.getX();
	}

	/**
	 * 
	 * {@inheritDoc}
	 */
	public double getVelY() {
		return velocity.getY();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public double getModule() {
		return velocity.magnitude();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public Velocity2D getNormalized() {
		public velocity.normalize();
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public void stop() {
		this.velocity = new Point2D(0, 0);
	}
	
	/**
	 * 
	 * {@inheritDoc}
	 */
	public void updateVelocity(Velocity2D newVelocity) {
		this.velocity = newVelocity;
	}
	
}
