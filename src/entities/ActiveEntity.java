package entities;
import entities.utils.HealtPoints;

public abstract class ActiveEntity extends AbstractEntity implements HealthPoints {
	
	private final static int MIN_HP = 0; 
    private int hp;
    private int maxHp;
    private Velocity2D vel;
    
    public ActiveEntity(final Velocity2D vel, final int maxHp) {
        this.hp = maxHp;
        this.maxHp = maxHp;
        this.vel = vel;
    }

    //move methods
	

    // In this.position conosciamo già la posizione di questo ActiveEntity
    // Tramite move gli diamo una velocità, grazie alla quale si muovere
    // Alla fine del movimento si trovera in una nuova posizione -> che salveremo in this.position e restituiremo tramite il metodo

    
    public Point2D move(final Velocity2D vel) {
        
    }

    public final int getHP() {
        return this.hp;
    }
    
    public final int getMaxHP() {
        return this.maxHp;
    }
	
    public final void increaseHP(final int increase) {
        this.hp = (this.hp + increase) < this.maxHp ? (this.hp + increase) : this.maxHp;
    }
    
    public final void decreaseHP(final int decrease) {
        this.hp = (this.hp - decrease) > MIN_HP ? (this.hp - decrease) : MIN_HP;
    }
}
