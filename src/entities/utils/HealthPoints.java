package entities;

public interface HealthPoints {

	int getHP();
	int getMaxHP();
	void increaseHP(int increase);
	void decreaseHP(int decrease);
}
