package entities;

import javafx.geometry.Point2D;

import javafx.scene.shape.Shape;

//walls - obstacles - items
public abstract class AbstractEntity {
	
	protected Point2D position;
	protected final Shape shape;
	
	public AbstractEntity(final Point2D p2d, final Shape shape) {
	    this.position = p2d;
	    this.shape = shape;
	}
	
	public final Point2D getPosition() {
	    return this.position;
	}
	
	public final Shape getShape() {
	    return this.shape;
	}

	// Ritornerà qualcosa, da definire
	public abstract void hit();

}
